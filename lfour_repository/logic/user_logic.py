
from core.db import *
import datetime
from lfour_repository.helper.util import *


class UserLogic:

    @staticmethod
    def get_roles_dict():
        db_agent = get_db_instance()
        roles = db_agent.select(Role)
        roles_dict = dict()
        if roles.count() > 0:
            for role in roles:
                roles_dict[role.id] = role.role_name
        return roles_dict

    @staticmethod
    def get_user_roles_dict():
        db_agent = get_db_instance()
        user_roles = db_agent.select(UserRole)
        user_roles_dict = dict()
        if user_roles.count() > 0:
            for user_role in user_roles:
                user_roles_dict[user_role.user.id] = user_role.role.role_name
        return user_roles_dict

    @staticmethod
    def insert_user_roles(user_id,roles):
        db_agent = get_db_instance()
        user = db_agent.select_first(User,User.id==user_id)
        for r_id in roles:
            role = db_agent.select_first(Role,Role.id==r_id)
            user_role_new = UserRole()
            user_role_new.user = user
            user_role_new.role = role
            user_role_new.note = ""
            user_role_new.modified = datetime.datetime.today()
            user_role_new.created = datetime.datetime.today()
            db_agent.insert(user_role_new)

    @staticmethod
    def update_user_roles(user_id, new_user_roles):
        db_agent = get_db_instance()
        user = db_agent.select_first(User,User.id==user_id)
        original_roles = Util.get_list_from_record_set(
            get_db_instance().get_record_set("select role_id from user_role where user_id={0}".format(user_id)))
        original_roles = [int(x) for x in original_roles]
        changed = False
        today = datetime.datetime.today()
        role_list = new_user_roles
        role_list = [int(x) for x in role_list]
        # Compare new role list with original role list
        roles_deleted = list(set(original_roles) - set(role_list))
        for r_id in roles_deleted:
            delete_user_role = db_agent.select_first(UserRole,UserRole.role_id==r_id,UserRole.user_id==user_id)
            db_agent.delete(delete_user_role)
            changed = True

        roles_added = list(set(role_list) - set(original_roles))
        for r_id in roles_added:
            role = db_agent.select_first(Role,Role.id==r_id)
            new_user_role = UserRole()
            new_user_role.user = user
            new_user_role.role = role
            new_user_role.created = datetime.datetime.today()
            new_user_role.modified = datetime.datetime.today()
            db_agent.insert(new_user_role)
            changed = True

        return changed

    @staticmethod
    def is_user_exist(login_id):
        db_agent = get_db_instance()
        user = db_agent.select(User,User.login_id==login_id)
        if user.count() > 0:
            return True
        return False