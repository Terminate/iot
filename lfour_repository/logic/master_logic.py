
from core.mongoagent import agent
from lfour_repository.helper.util import *

class MasterLogic:

    @staticmethod
    def get_combobox_dict(collection_name):
        command = "agent.db." + collection_name + ".find()"
        list_info = list(eval(command))
        list_info = agent.convert_cursor(list_info)
        dict_info = dict()
        if list_info.__len__() > 0:
            for item in list_info:
                dict_info[item['id']] = item['name']
        return dict_info

    @staticmethod
    def get_combobox_dict_special(collection_current, collection_back):
        command = "agent.db." + collection_current + ".find()"
        list_info = list(eval(command))
        list_info = agent.convert_cursor(list_info)
        dict_info = dict()
        if list_info.__len__() > 0:
            for item in list_info:
                dict_info[item['id']] = item[collection_back]['name'] + '/' + item['name']
        return dict_info

    @staticmethod
    def get_max_id(collection_name, field_sort):
        max_id = 0
        command = "agent.db." + collection_name + ".find().sort([('" + field_sort + "', -1)]).limit(1)"
        object_command = eval(command)
        if object_command is not None and object_command.count() != 0:
            max_id = int(list(object_command)[0][field_sort])
        return max_id
