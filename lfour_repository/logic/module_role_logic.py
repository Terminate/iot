from core.db import *
from core.const import *
from sqlalchemy.orm import load_only


class ModuleRoleLogic:

    @staticmethod
    def get_roles_name(role_id):
        db_agent = get_db_instance()
        role_name = db_agent.execute("select name from role where id={0}".format(role_id))
        return role_name

    @staticmethod
    def get_module_info_dict():
        db_agent = get_db_instance()
        module_infos = db_agent.select(ModuleInfo)
        module_info_dict = dict()
        if module_infos.count() > 0:
            for module_info in module_infos:
                module_info_dict[module_info.id] = module_info.module_name
        return module_info_dict

    @staticmethod
    def get_current_module_info_dict(current_role_id):
        db_agent = get_db_instance()
        current_modules = db_agent.select(RoleRight,RoleRight.role_id==current_role_id)
        current_role_id = []
        if current_modules.count() > 0:
            for current_module in current_modules:
                current_role_id.append(current_module.module_id)
        module_infos = db_agent.select(ModuleInfo)
        module_info_dict = dict()
        if module_infos.count() > 0:
            for module_info in module_infos:
                if module_info.id not in current_role_id:
                    module_info_dict[module_info.id] = module_info.module_name
        return module_info_dict