from flask import Flask
app = Flask(__name__)
from lfour_repository.views.index import *
from lfour_repository.template_tags.template_tags import *
from lfour_repository.views.login import *
from lfour_repository.views.master import *


