function createCurrentGauges(chartId, tempValue, humValue){
    // based on prepared DOM, initialize echarts instance
    var chart = echarts.init(document.getElementById(chartId));

    // specify chart configuration item and data
    option = {
        series: [
            {
                name: 'Temperature',
                type: 'gauge',
                center: ['33%', '50%'],    // Default global centering
                radius: '70%',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 110,
                detail: {
                    show: true,
                    offsetCenter: ['0%',40],
                    formatter:function(v) {
                        if (isNaN(v))
                        {
                            return "";
                        }else{
                            return v + ' °C';
                        }
                    },
                    //formatter: '{value} °C',
                    textStyle: {
                        //color: 'black',
                        fontSize: 30,
                        //fontWeight: bold,
                    }
                },
                data: [{value: tempValue, name: 'Temperature'}],
                title: {
                    show: false,
                    offsetCenter: ['0%', 20],
                    //textStyle: {
                    //  color: '#333',
                    //  fontSize: 15
                    //}
                },
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: [
                            [0.2, '#0000cc'],
                            [0.3, '#009900'],
                            [0.7, '#ff9900'],
                            [1, '#ff0000']
                        ],
                        width: 15
                    }
                },
                splitLine: {
                    length: 20,
                    lineStyle: {
                        color: 'auto'
                    }
                },
                axisLabel: {
                    show: true,
                    formatter: function(v) {
                      switch (v + '') {
                      case '0':
                          return '0 °C';
                      //case '20':
                      //    return '20';
                      case '30':
                          return '30';
                      //case '70':
                      //    return '70';
                      //case '100':
                      //    return '100 °C';
                      default:
                          return '';
                      }
                    },
                    textStyle: {
                        fontSize: 15,
                    }
                },
                pointer: {
                    width:4,
                },
            },
            {
                name: 'Humidity',
                type: 'gauge',
                min: 0,
                max: 100,
                center: ['60%', '50%'],
                radius: '70%',
                startAngle: 430,
                endAngle: 360,
                splitNumber: 2,
                axisLine: {
                    lineStyle: {
                        color: [
                          [0.25, '#C5CAE9'],
                          [0.50, '#00cc66'],
                          [0.75, '#ff6633'],
                          [1, '#ff0000']
                        ],
                        width: 15
                    }
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    formatter:function(v){
                        switch (v + '') {
                            case '0' : return '0 %';
                            case '100' : return '100 %';
                        }
                    },
                    textStyle: {
                        fontSize: 15,
                    }
                },
                splitLine: {
                    length: 15,
                    lineStyle: {
                        color: 'auto'
                    }
                },
                pointer: {
                    width:4
                },
                title: {
                    show: false
                },
                detail: {
                  show: true,
                  offsetCenter: ['50%', 40],
                  formatter:function(v) {
                      if (isNaN(v))
                      {
                        return "";
                      }else{
                        return v + ' %';
                      }
                  },
                  textStyle: {
                    //color: 'black',
                    fontSize: 30,
                    //fontWeight: bold,
                  }
                },
                data:[{value: humValue, name: 'Humidity'}]
            },
        ]
    };

    // use configuration item and data specified to show chart
    chart.setOption(option);
    chart.resize();

}

function drawTrendGraph(chartId, loc, range){
    // Parameter
    $.ajax({url: "/graph_ajax?range=" + range + "&loc=" + loc, success: function(result){
        result[1].splice(0, 0, "Temperature");
        result[2].splice(0, 0, "Humidity");
        result[3].splice(0, 0, "x");
        var height = $(window).height() - 80;

        var chart = echarts.init(document.getElementById(chartId));

        option = {
            legend: {
                data:['Temperature','Humidity']
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: result[3]
            },
            yAxis: [

                {
                    type: 'value',
                    name: 'Temperature',
                    min: result[100][0], //'dataMin',
                    max: result[100][1], //'dataMax',
                    nameTextStyle: {
                        color: 'red'
                    },
                    splitLine:{
                        //show:true,
                    },
                    axisLabel: {
                        textStyle:{
                            color:'#000',
                        },
                        formatter: '{value} °C'
                    },
                },
                {
                    type: 'value',
                    name: 'Humidity',

                    min: result[100][2],//'dataMin',
                    max: result[100][3],//'dataMax',

                    nameTextStyle: {
                        color: 'blue'
                    },
                    axisLabel: {
                        //interval:'0',
                        textStyle:{
                            color:'#000',
                        },
                        formatter: '{value} %'
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: ['#cccc00'],
                        }
                    },
                    //max:800000,
                    splitNumber:5
                },
            ],
            series: [
                {
                    color:"#ffcc99",
                    name:'s_min',
                    data: result[10],
                    type: 'line',
                    areaStyle: {},
                    smooth: true,
                },
                {
                    color:"#ffcc99",
                    name:'s_max_base',
                    stack:'s_stack',
                    data: result[12],
                    type: 'line',
                    smooth: true,
                },
                {
                    color:"#ffcc99",
                    name:'s_stack',
                    stack:'s_stack',
                    data: result[11],
                    type: 'line',
                    areaStyle: {},
                    smooth: true,
                },


                {
                color:"red",
                name:'Temperature',
                data: result[1],
                type: 'line',
                //areaStyle: {},
                lineStyle: {
                    width: 5,
                },
                smooth: true,
                },

                {
                name:'Humidity',
                color:"blue",
                data: result[2],
                type: 'line',
                yAxisIndex:1,
                lineStyle: {

                    width: 5,
                },
                smooth: true,
                },
            ]
        };
        chart.setOption(option);
        chart.resize();

    }});

}

function drawMiniTrendGraph(chartId, loc, range){
    // Parameter
    $.ajax({url: "/graph_ajax?range=" + range + "&loc=" + loc, success: function(result){
        result[1].splice(0, 0, "Temperature");
        result[2].splice(0, 0, "Humidity");
        result[3].splice(0, 0, "x");
        var height = $(window).height() - 80;

        var chart = echarts.init(document.getElementById(chartId));

        option = {
            legend: {
                show: false,
                //data:['Temperature','Humidity']
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: result[3]
            },
            yAxis: [
                {
                    type: 'value',
                    name: '°C',
                    min: result[100][0], //'dataMin',
                    max: result[100][1], //'dataMax',
                    nameTextStyle: {
                        color: 'red'
                    },
                    splitLine:{
                        //show:true,
                    },
                    axisLabel: {
                        textStyle:{
                            color:'red',
                            fontSize: 10,
                        },
                        formatter: '{value}'
                    },
                },
                {
                    type: 'value',
                    name: '%',
                    min: result[100][2], //'dataMin',
                    max: result[100][3], //'dataMax',
                    nameTextStyle: {
                        color: 'blue'
                    },
                    axisLabel: {
                        //interval:'0',
                        textStyle:{
                            color:'blue',
                            fontSize: 10,
                        },
                        formatter: '{value}'
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: ['#eeeeee'],
                        }
                    },
                    splitNumber:5
                }
            ],
            series: [
                {
                    color:"#ffcc99",
                    name:'s_min',
                    data: result[10],
                    type: 'line',
                    areaStyle: {},
                    smooth: true,
                },
                {
                    color:"#ffcc99",
                    name:'s_max_base',
                    stack:'s_stack',
                    data: result[12],
                    type: 'line',
                    smooth: true,
                },
                {
                    color:"#ffcc99",
                    name:'s_stack',
                    stack:'s_stack',
                    data: result[11],
                    type: 'line',
                    areaStyle: {},
                    smooth: true,
                },

                {
                color:"red",
                name:'Temperature',
                data: result[1],
                type: 'line',
                //areaStyle: {},
                lineStyle: {
                    width: 5,
                },
                smooth: true,
                },

                {
                name:'Humidity',
                color:"blue",
                data: result[2],
                type: 'line',
                yAxisIndex:1,
                lineStyle: {

                    width: 5,
                },
                smooth: true,
                },
            ]
        };
        chart.setOption(option);
        chart.resize();

    }});

}

function drawCurrentGauges(chartId, loc) {
    $.ajax({url: "/current_ajax?loc=" + loc,
        success: function(result){
            var tempValue = null;
            var humValue = null;
            if (result[0] > -1000){
                tempValue = result[0];
            }
            if (result[1] > -1000){
                humValue = result[1];
            }
            createCurrentGauges(chartId, tempValue, humValue);
        }
    });
}
function drawCurrentValues(tempId, humId, loc) {
    $.ajax({url: "/current_ajax?loc=" + loc,
        success: function(result){
            var tempValue = null;
            var humValue = null;
            if (result[0] > -1000){
                tempValue = result[0];
                var temp = $('#' + tempId);
                temp.html(tempValue + " °C");
                if (tempValue < 22){
                    temp.css('color', 'blue');
                }else if (tempValue < 28){
                    temp.css('color', 'green');
                }else if (tempValue < 50){
                    temp.css('color', 'orange');
                }else{
                    temp.css('color', 'red');
                }
            }
            if (result[1] > -1000){
                humValue = result[1];
                var temp = $('#' + humId);
                temp.html(humValue + " %");
                if (humValue < 22){
                    temp.css('color', 'blue');
                }else if (humValue < 28){
                    temp.css('color', 'green');
                }else if (humValue < 50){
                    temp.css('color', 'orange');
                }else{
                    temp.css('color', 'red');
                }
            }

        }
    });
}





















// Khong can xai
function drawGauges2(chart_data){
    chart_data.forEach(function(item,index){
        if (item[1] > -1000){
            drawHumiGauge(index,item[1])
        }else{

            $( "#chart" + index  ).html("");
        }
        if (item[0] > -1000){
            drawTempGauge(index,item[0])
        }else{

            $( "#temp_chart" + index  ).html("");
        }
    });
}

function drawTempGauge3(index, value){
    var opts = {
      angle: 0.15, // The span of the gauge arc
      lineWidth: 0.44, // The line thickness
      radiusScale: 1, // Relative radius
      pointer: {
        length: 0.6, // // Relative to gauge radius
        strokeWidth: 0.035, // The thickness
        color: '#000000' // Fill color
      },
      limitMax: false,     // If false, max value increases automatically if value > maxValue
      limitMin: false,     // If true, the min value of the gauge will be fixed
      colorStart: '#6FADCF',   // Colors
      colorStop: '#8FC0DA',    // just experiment with them
      strokeColor: '#E0E0E0',  // to see which ones work best for you
      generateGradient: true,
      highDpiSupport: true,     // High resolution support

    };
    var target = document.getElementById('canvas' + index); // your canvas element
    var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = 3000; // set max gauge value
    gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
    gauge.animationSpeed = 32; // set animation speed (32 is default value)
    gauge.set(value); // set actual value
}

function drawGraph2(){
    		var chart = c3.generate({
    		bindto:"#chart",
			data: {
				x : 'x',
				columns: [
					['x', 'www.somesitename1.com', 'www.somesitename2.com', 'www.somesitename3.com', 'www.somesitename4.com', 'www.somesitename5.com', 'www.somesitename6.com', 'www.somesitename7.com', 'www.somesitename8.com', 'www.somesitename9.com', 'www.somesitename10.com', 'www.somesitename11.com', 'www.somesitename12.com'],
					['pv', 90, 100, 140, 200, 100, 400, 90, 100, 140, 200, 100, 400, 90, 100, 140, 200, 100, 400, 90, 100, 140, 200, 100, 400, 90, 100, 140, 200, 100, 400, 90, 100, 140, 200, 100, 400,90, 100, 140, 200, 100, 400, 90, 100, 140, 200, 100, 400],
				],
				type: 'area-spline'
			},
		    size: {
                height:400
            },
			axis: {
			      y: {
                    max: 100,
                    min: 0,
                    // Range includes padding, set 0 if no padding needed
                    padding: {top:0, bottom:0, left:0}
                },
				x: {
					/*type: 'category',
					tick: {
						//rotate: 0,
						values: [3,7],
						multiline: false
					},
					height: 130,
					*/

					                    type: 'category', //indexed
                    tick: {
                        // this also works for non timeseries data
                        values: [2,5],
                        rotate: 180
                    },
                    height: 80


				}
			}
		});
}

function drawGraph1(){
    // Parameter
    var loc = $( "#location" ).val();
    var range = $( "#range" ).val();
    $.ajax({url: "/graph_ajax?range=" + range + "&loc=" + loc, success: function(result){
        result[1].splice(0, 0, "Temperature");
        result[2].splice(0, 0, "Humidity");
        result[3].splice(0, 0, "x");
        var height = $(window).height() - 80;
        //result[3] =  ['x', 'www.somesitename1.com', 'www.somesitename2.com', 'www.somesitename3.com', 'www.somesitename4.com', 'www.somesitename5.com', 'www.somesitename6.com', 'www.somesitename7.com', 'www.somesitename8.com', 'www.somesitename9.com', 'www.somesitename10.com', 'www.somesitename11.com', 'www.somesitename12.com'];

        //alert(result[1]);
        //alert(result[3]);
        var chart = c3.generate({
            bindto:"#chart",
            data: {
                x: 'x',
                columns: [
                    result[3],
                    result[1],
                    result[2]
                ],
                types: {
                    Temperature: 'area-spline',
                    Humidity: 'area-spline'
                }
            },
            size: {
                height:height
            },
            axis: {
                y: {
                    max: 100,
                    min: 0,
                    // Range includes padding, set 0 if no padding needed
                    padding: {top:0, bottom:0, left:0}
                },
                x: {
                    type: 'category', //indexed category - category se gom may cai trung lai
                    tick: {
                        // this also works for non timeseries data
                        values: result[4],
                        //rotate: 180,
                        multiline: false // Khong cho chu xuong hang thanh doc
                    },
                    height: 80


                }
            }
        });



    }});



}

function drawTempGauge1(index, value){
    $('#temp_chart' + index).html(value);
    //return;
    $('#temp_chart' + index).kumaGauge({
        value : value,
        fill : '#F34A53',
        gaugeBackground : '#1E4147',
        gaugeWidth : 10,
        showNeedle : false,
        label : {
        display : true,
        left : '0',
        right : '100',
        fontColor : '#1E4147',
        fontSize : '11',
        fontWeight : 'bold'
        }
    });
}

function drawTempGauge2(index, value){
   $('#temp_chart' + index).html("");
    //alert($('#temp_chart' + index).html());
   $.jqplot('temp_chart' + index ,[[value]],{
        grid: {
           background: "transparent"
       },
       seriesDefaults: {
           renderer: $.jqplot.MeterGaugeRenderer,
           rendererOptions: {
               label: 'Temperature ' + value + ' °C',
               labelPosition: 'bottom',
               labelHeightAdjust: -5,
               ticks: [0, 25, 50, 75 ,100],
               intervals:[20, 30, 50, 70, 100],
               intervalColors:['#3366ff','#66cc66', '#E7E658', 'orange', 'red']
           }
       }
   });
}

function drawTempGauge(index, value){
   //var test = $('#temp_chart' + index).html();
   //if (test != "") {
   //     option.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;
   //     myChart.setOption(option, true);
   //}

        var myChart = echarts.init(document.getElementById('temp_chart' + index));

        // specify chart configuration item and data
        option = {
            tooltip : {
                formatter: "{a} <br/>{b} : {c}%"
            },

            series: [
                {
                    name: 'Temperature',
                    type: 'gauge',
                    detail: {formatter:'{value} °C', offsetCenter: ['0%', 20],
                            textStyle: {
                                color: 'auto',
                                fontSize: 15
                              }},
                    data: [{value: value, name: 'Temperature'}],
                    title: {
                      show: false,
                      offsetCenter: ['0%', 20],
                      textStyle: {
                      //  color: '#333',
                        fontSize: 15
                      }
                    },

                    axisLine: {
                      show: true,
                      lineStyle: {
                        color: [
                          [0.2, '#0033cc'],
                          [0.3, '#009900'],
                          [0.7, '#ff9900'],
                          [1, '#ff0000']
                        ],
                        width: 30
                      }
                    },

                    axisLabel: {
                      show: true,
                      formatter: function(v) {
                          switch (v + '') {
                          case '0':
                              return '0';//1955
                          case '20':
                              return '20';//3910
                          case '40':
                              return '40';//5865
                          case '60':
                              return '60';//7820
                          case '80':
                              return '80';
                          case '100':
                              return '100';
                          default:
                              return '';
                          }
                      },
                      //textStyle: {
                       // fontSize: 20,
                     // }
                    },

                }
            ]
        };

        // use configuration item and data specified to show chart
        myChart.setOption(option);
}

function drawHumiGauge(index, value){
    c3.generate({
        bindto:"#chart" + index,
        data: {
            columns: [
                ['Humidity', value]
            ],
            type: 'gauge'
        },
        gauge: {
            label: {
                //format: function(value, ratio) {
                //    return value;
                //},
                show: true // to turn off the min/max labels.
            },
        //min: 0, // 0 is default,
        //max: 100, // 100 is default
        //units: ' C', //can handle negative min e.g. vacuum / voltage / current flow / rate of change
        //width: 39 // for adjusting arc thickness
        },
        color: {
            pattern: ['lightblue','green', 'orange', 'red'], // the three color levels for the percentage values.
            threshold: {
                //unit: 'value', // percentage is default
                //max: 200, // 100 is default
                values: [20, 30, 50, 100]
            }
        },
        size: {
            height: "100%"
        }
    });
}

