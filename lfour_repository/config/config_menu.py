# -*- coding: utf-8 -*-

from enum import Enum
from .config_role import RoleName


class ScreenName(Enum):
    """
    Screen name define
    """
    Home = 1
    Master = 26

"""
    Screen name and role name mapping
"""
config_menus_roles = {
    ScreenName.Home: [RoleName.Other],
    ScreenName.Master: [RoleName.Other],

}


"""
Menu define
"""

config_menus = [
    {'icon': "fa-home", "name": "Home", "url": "/", "screen_name": ScreenName.Home},
    {'icon': "fa-home", "name": "Master", "url": "/master", "screen_name": ScreenName.Master},

]
