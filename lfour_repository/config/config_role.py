# -*- coding: utf-8 -*-

from enum import Enum
# from .config_common import AuthMetaTypeDefine
"""
Define role and right of user
"""

class UserRightType(Enum):
    View = 1
    Update = 2
    Delete = 4
    Insert = 8
    All = 15


class RoleName(Enum):
    Admin = 1
    Manager = 2
    Sales = 3
    Accounting = 4
    Other = 0


class ModuleName(Enum):
    Meta = 1
    AuthMeta = 2
    User = 3
    Account = 4
    Fiscal = 5
    BizProspect = 11
    BizBudget = 12
    Budget = 13
    JournalVoucher = 14
    ScheduledJournal = 15
    Bill = 16
    Loan = 17
    BudgetPerformance = 21
    CashFlow = 22
    Strac = 23


# config_all_filter = [AuthMetaTypeDefine.Department.code]

"""
Define view belong to which module
"""
config_view_module_mapping = {
    "index": None,
    "meta": ModuleName.Meta.value,
    "account": ModuleName.Account.value,
    "auth_meta": ModuleName.AuthMeta.value,
    "user": ModuleName.User.value,
    "budget_performance": ModuleName.BudgetPerformance.value,
    "budget_performance_detail": ModuleName.BudgetPerformance.value,
    "cash_flow": ModuleName.CashFlow.value,
    "prospect": ModuleName.BizProspect.value,
    "biz_budget": ModuleName.BizBudget.value,
    "fiscal": ModuleName.Fiscal.value,
    "journal_voucher": ModuleName.JournalVoucher.value,
    "scheduled_journal": ModuleName.ScheduledJournal.value,
    "budget": ModuleName.Budget.value,
    "strac": ModuleName.Strac.value,
}


"""
Define which module user can access
"""
config_roles = {
    ModuleName.Meta: {
        RoleName.Admin: {'right': UserRightType.All.value, 'filter':  None},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': None}
    },

    ModuleName.AuthMeta: {
        RoleName.Admin: {'right': UserRightType.All.value, 'filter': None},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': None}
    },

    ModuleName.Account: {
        RoleName.Admin: {'right': UserRightType.All.value, 'filter': None},
        RoleName.Accounting: {'right': UserRightType.View.value | UserRightType.Update.value, 'filter': None},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': None}
    },

    ModuleName.Fiscal: {
        RoleName.Admin: {'right': UserRightType.All.value, 'filter': None},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': None},
    },

    ModuleName.User: {
        RoleName.Admin: {'right': UserRightType.All.value, 'filter': None},
        RoleName.Manager: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': [1]},
    },

    ModuleName.BizBudget: {
        RoleName.Other: {'right': UserRightType.View.value, 'filter': [1]},
        RoleName.Manager: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Admin: {'right': UserRightType.All.value, 'filter': None}
    },

    ModuleName.BizProspect: {
        RoleName.Sales: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Manager: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Other: {'right': UserRightType.All.value, 'filter': [1]}
    },

    ModuleName.Budget: {
        RoleName.Other: {'right': UserRightType.View.value, 'filter': [1]},
        RoleName.Manager: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Admin: {'right': UserRightType.All.value, 'filter': None}
    },

    ModuleName.JournalVoucher: {
        RoleName.Accounting: {'right': UserRightType.All.value, 'filter': None},
        RoleName.Manager: {'right': UserRightType.View.value, 'filter': None},
    },

    ModuleName.ScheduledJournal: {
        RoleName.Sales: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Manager: {'right': UserRightType.All.value, 'filter': [1]},
        RoleName.Other: {'right': UserRightType.All.value, 'filter': [1]}
    },

    ModuleName.BudgetPerformance: {
        RoleName.Manager: {'right': UserRightType.View.value, 'filter': None},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': [11]}
    },

    ModuleName.CashFlow: {
        RoleName.Manager: {'right': UserRightType.View.value, 'filter': None},
        RoleName.Other: {'right': UserRightType.View.value, 'filter': [1]}
    },
}
