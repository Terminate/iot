from enum import Enum



class ModuleUserType(Enum):
    Strategy = (1, "UI")
    InterLocking = (10, "API")

    def __init__(self, code, title):
        self.code = code
        self.title = title

    @staticmethod
    def get_dict():
        """
        Enum format: name = value(code,title)
        Return value
        """
        enum_dict = {}
        for e in ModuleUserType:
            enum_dict[e.code] = e.title
        return enum_dict


class NetworkProvider(Enum):
    Sonet = (1, "So-net")

    def __init__(self, code, title):
        self.code = code
        self.title = title

    @staticmethod
    def get_dict():
        """
        Enum format: name = value(code,title)
        Return value
        """
        enum_dict = {}
        for e in NetworkProvider:
            enum_dict[e.code] = e.title
        return enum_dict

class ModuleRoleRightType(Enum):
    View = (1,"View")
    Insert = (2,"Insert")
    Update = (4,"Update")
    Delete = (8,"Delete")

    def __init__(self, code, title):
        self.code = code
        self.title = title

    @staticmethod
    def get_dict():
        """
        Enum format: name = value(code,title)
        Return value
        """
        enum_dict = {}
        for e in ModuleRoleRightType:
            enum_dict[e.code] = e.title
        return enum_dict