from flask import render_template
from lfour_repository import app
from core.mongoagent import agent
import numpy as np
from collections import defaultdict
from lfour_repository.config.config_menu import *


@app.route("/")
def index():
    a = agent.db.monitor_group.find().sort([("name", 1)])
    group = list()
    for l in a:
        i = list()
        i.append(l['id'])
        i.append(l['name'])
        group.append(i)

    loc = get_factory_list()
    context = {'loc': loc, 'group': group}

    return render_template("index.html", screen_name=ScreenName.Home, context=context)


@app.route("/current_ajax")
def iot_ajax_view():
    import json
    from flask import Response

    from flask import request
    loc = request.args.get('loc')
    if loc is None or not loc.isdigit():
        loc = -1
    else:
        loc = int(loc)

    # import random
    # b = [[random.randint(5, 50),random.randint(5, 50)],[random.randint(5, 50),random.randint(5, 50)],[random.randint(5, 50),random.randint(5, 50)],[random.randint(5, 50),random.randint(5, 50)]]

    data = get_current_data(loc)

    js = json.dumps(data)

    resp = Response(js, status=200, mimetype='application/json')

    return resp


@app.route("/current")
def iot_view():
    from flask import request
    monitor_id = request.args.get('monitor')
    if monitor_id is None or not monitor_id.isdigit():
        monitor_id = -1
    else:
        monitor_id = int(monitor_id)

    loc_ids, loc_data = get_monitor_locations(monitor_id)

    # loc_data = [{'id': 1, 'name': "Testing location 1"},
    #             {'id': 2, 'name': "Testing location 2"}]
    # loc_ids = [1, 2, 3, 4, 6, 8, 1]
    print(loc_data)

    count = len(loc_ids)
    cols = 1
    rows = 1

    if count < 2:
        pass
    elif count == 2:
        cols = 2
        rows = 1
    elif count <= 4:
        cols = 2
        rows = 2
    elif count <= 6:
        cols = 3
        rows = 2
    elif count <= 8:
        cols = 4
        rows = 2
    elif count <= 12:
        cols = 4
        rows = 3
    elif count <= 15:
        cols = 5
        rows = 3
    else:
        cols = 6
        rows = 3

    col_w = int(12/cols)
    row_h = int(12/rows)

    context = {'loc_data': loc_data, 'loc_ids':loc_ids, 'cols': cols, 'rows': rows, 'row_h': row_h, 'col_w': col_w}

    return render_template("current.html",
                           context=context)


@app.route("/notify")
def notify_view():
    # db_access = get_db_instance()
    # context = {'users': "aaa"}
    # return jsonify(context)
    import json
    from flask import Response

    a = agent.db.notify_setting.find()
    b = agent.convert_cursor(a)

    js = json.dumps(b)

    resp = Response(js, status=200, mimetype='application/json')

    return resp


@app.route("/graph")
def graph_view():
    from flask import request
    loc_id = request.args.get('loc')
    if loc_id is None or not loc_id.isdigit():
        loc_id = -1
    else:
        loc_id = int(loc_id)

    loc = get_factory_list()
    context = {'loc': loc, 'loc_id': loc_id}
    return render_template("graph.html", context=context)


@app.route("/graph_ajax")
def graph_ajax_view():
    import json
    from flask import Response
    from flask import request
    loc = request.args.get('loc')
    period = request.args.get('range')
    if loc is None or not loc.isdigit():
        loc = 1
    else:
        loc = int(loc)

    if period is None or not period.isdigit():
        period = 5
    else:
        period = int(period)

    points = int(round(period * 60 / 12,0))
    if points > 60:
        points = 60

    d = get_period_data(loc, period, points)

    if len(d) > 0 and len(d[1]) > 0:
        base = 1

        # Tim max min
        t_min = np.amin(d[1])
        t_max = np.amax(d[1])

        h_min = np.amin(d[2])
        h_max = np.amax(d[2])

        # tinh base cho humidity
        if h_max - h_min > 0 and t_max-t_min > 0:
            rate = (t_max-t_min) / (h_max - h_min)
            base_h = round(base/rate)
        else:
            base_h = base

        a_min = list()
        a_max = list()
        a_max_base = list()
        for i in d[1]:
            a_min.append(t_min)
            a_max.append(base)
            a_max_base.append(t_max)

        a_min.append(t_min)
        a_max.append(base)
        a_max_base.append(t_max)

        d[10] = a_min
        d[11] = a_max
        d[12] = a_max_base

        d[100] = [t_min - base, t_max + base, h_min - base_h, h_max + base_h]
    else:
        d[10] = []
        d[11] = []
        d[12] = []
        d[100] = [0, 0, 0, 0]

    print(d)

    js = json.dumps(d)
    resp = Response(js, status=200, mimetype='application/json')

    return resp


def get_factory_list():
    loc = agent.db.location.find()
    loc = agent.convert_cursor(loc)
    lv = list()
    for l in loc:
        lv.append([l["id"], l["factory"]["name"] + "/" + l["name"]])

    return lv


def get_monitor_locations(monitor_id):
    mon = agent.db.monitor_group_member.find({"monitor_group_id": monitor_id}). \
        sort([("sort", 1)])
    mon = agent.convert_cursor(mon)
    loc_ids = list()
    loc_data = list()
    # print(mon)
    for l in mon:
        loc_ids.append(l["location_id"])

        loc_data.append({"id": l["location_id"], "name": "{}/{}".format(l["location"]["factory"]["name"],
                                                                        l["location"]["name"])})
    return loc_ids, loc_data


def get_current_data(location_id=None):
    import datetime
    import time
    now = datetime.datetime.now()
    now = now - datetime.timedelta(seconds=5 * 60)
    now = time.mktime(now.timetuple())

    lv = list()
    a = agent.db.iot.find({"location_id": location_id, 'value': {'$gte': -10}, "sensor_type_id": 1, 'event_time': {'$gte': now}}). \
        sort([("event_time", -1)]).limit(1)
    if a.count() > 0:
        b = a.next()
        lv.append(b["value"])
    else:
        lv.append(-1000)

    a = agent.db.iot.find({"location_id": location_id, 'value': {'$gte': -10}, "sensor_type_id": 2, 'event_time': {'$gte': now}}). \
        sort([("event_time", -1)]).limit(1)

    if a.count() > 0:
        b = a.next()
        lv.append(b["value"])
    else:
        lv.append(-1000)

    return lv


def get_period_data(location_id, period=60, points=60, sample=10):
    import datetime
    import time
    # now = datetime.datetime.utcnow()
    now = datetime.datetime.now()
    start = now - datetime.timedelta(seconds=period * 60)

    t_start = time.mktime(start.timetuple())
    t_now = time.mktime(now.timetuple())

    d = agent.db.iot.find({"location_id": location_id, 'value': {'$gte': -10}, 'event_time': {'$gte': t_start, '$lt': t_now}}).sort(
        [("event_time", 1)])
    d = list(d)
    # point_a_point = round((period * 60 / points) / sample, 0)
    ret = dict()
    ret[1] = list()
    ret[2] = list()
    ret[3] = list()
    ret[4] = list()
    # ret[1].append("Temperature")
    # ret[2].append("Humidity")

    if len(d) < 1:
        return ret

    k = 0
    end = now
    start = now - datetime.timedelta(seconds=period * 60)
    plus = round((period * 60 / points), 0)

    axis_points = round(points / 10,0)
    prev = list()
    for i in range(1, 3):
        prev.append(0)

    for x in range(0, points):
        end = start + datetime.timedelta(seconds=plus)

        t_start = time.mktime(start.timetuple())
        t_end = time.mktime(end.timetuple())

        temp_list = list()
        while k < len(d) and d[k]["event_time"] < t_start:
            k += 1

        if k < len(d) and d[k]["event_time"] >= t_start:
            while k < len(d) and d[k]["event_time"] <= t_end:
                rec = d[k]
                temp_list.append(rec)
                k += 1

        sd = defaultdict(list)
        for rec in temp_list:
            sd[rec['sensor_type_id']].append(rec['value'])

        for i in range(1, 3):
            v1 = round(np.mean(sd[i]), 2) if len(sd[i]) > 0 else 0
            if v1 == 0:
                v1 = prev[i-1]
            else:
                prev[i-1] = v1
            #print("v={}".format(v))
            #print(v1)

            ret[i].append(v1)

        ret[3].append(start.strftime("%H:%M"))

        # for x in range(0, points):
        #     if x % axis_points == 0:
        #         ret[4].append(x)

        start = end

    return ret


def get_current_data2():
    import json
    from flask import Response

    loc = agent.db.location.find()
    loc = agent.convert_cursor(loc)
    loc_value = list()
    import datetime
    import time
    now = datetime.datetime.now()
    now = now - datetime.timedelta(seconds=5 * 60)
    now = time.mktime(now.timetuple())
    for l in loc:
        lv = list()
        a = agent.db.iot.find({"location_id": l["id"], "sensor_type_id": 1, 'event_time': {'$gte': now}}).\
            sort([("event_time", -1)]).limit(1)
        if a.count() > 0:
            b = a.next()
            lv.append(b["value"])
        else:
            lv.append(-1000)

        a = agent.db.iot.find({"location_id": l["id"], "sensor_type_id": 2, 'event_time': {'$gte': now}}).\
            sort([("event_time", -1)]).limit(1)

        if a.count() > 0:
            b = a.next()
            lv.append(b["value"])
        else:
            lv.append(-1000)

        loc_value.append(lv)

    return loc, loc_value

