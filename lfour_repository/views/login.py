from lfour_repository import app
from flask import render_template
from flask import request
from core.auth_ui import *
from core.auth import *
from lfour_repository.config.config_message import *

@app.route("/login",methods=["GET","POST"])
def login():
    print("aa")
    error_message = ""
    if request.method == "POST":
        data = request.form

        user_id = data.get("user_id", "")
        password = data.get("password", "")
        # db_agent = get_db_instance()
        # user = db_agent.select_first(User, User.login_id == username)
        # if user is not None:
        if AuthUi.login(user_id, password):
                return redirect("/")
        else:
            error_message = Message.UsernamePasswordWrong.value
        # else:
        #     error_message = Message.UsernamePasswordWrong.value
    return render_template("login.html", error_message=error_message)


@app.route("/logout/")
def logout():
    AuthUi.logout()
    return redirect("/")

