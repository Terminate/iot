from lfour_repository import app
from lfour_repository.logic.master_logic import *
from lfour_repository.config.config_menu import *


@app.route("/master", methods=["GET", "POST"])
def master_view():
    from flask import render_template, request, redirect
    tab_index = 0

    if request.args.get('tab_index') is not None:
        tab_index = int(request.args.get('tab_index'))

    factory_link_id = 0
    if request.args.get('factory_id') is not None:
        factory_link_id = int(request.args.get('factory_id'))

    location_link_id = 0
    if request.args.get('location_id') is not None:
        location_link_id = int(request.args.get('location_id'))

    group_link_id = 0
    if request.args.get('group_id') is not None:
        group_link_id = int(request.args.get('group_id'))

    #region Factory form

    factory_form = GacoiForm('master_factory', '/master?tab_index=0&factory_id=' + str(factory_link_id), 'POST')
    factory_form.set_col_database(["id, factory_name, factory_note", "id, name, note"])
    factory_form.set_view("id, factory_name, factory_note")
    factory_form.set_key("id")

    factory_form.set_insert("factory_name, factory_note")
    factory_form.set_update("factory_name, factory_note")
    factory_form.set_option_deletable(True)

    factory_form.set_search('factory_name, factory_note')
    factory_form.set_order('factory_name, factory_note')
    factory_form.get_field('id').set_link('master?tab_index=1&factory_id=[id]')
    factory_form.set_required('factory_name')

    factory_form.init(request)

    factory_form.set_caption(["id, factory_name, factory_note", "ID,Name,Note"])

    if factory_form.is_action(GacoiFormAction.InsertDone):
        name = factory_form.get_field("factory_name").get_value()
        new_id = 1
        if agent.db.factory.count() != 0:
            new_id = int(list(agent.db.factory.find().sort([("_id", -1)]).limit(1))[0]['id']) + 1

        note = factory_form.get_field("factory_note").get_value()
        data_insert = {
            'id': new_id,
            'name': name,
            'note': note
        }
        agent.db.factory.insert_one(data_insert)

    if factory_form.is_action(GacoiFormAction.DeleteDone):
        current_id = int(factory_form.get_key_value("id"))
        data_delete = {
            'id': current_id
        }
        agent.db.factory.delete_one(data_delete)
        #find any location with factory_id
        delete_location_object = agent.db.location.find_one({'factory_id': current_id})
        if delete_location_object is not None:
            agent.db.location.remove({'factory_id': current_id})
            delete_location_id = int(dict(delete_location_object)['id'])
            agent.db.monitor_group_member.remove({'location_id': delete_location_id})
        #TO DO delete iot collection

    if factory_form.is_action(GacoiFormAction.UpdateDone):
        current_id = int(factory_form.get_key_value("id"))
        name = factory_form.get_field("factory_name").get_value()
        note = factory_form.get_field("factory_note").get_value()

        update_data = agent.db.factory.find_one({'id': current_id})
        update_data['name'] = name
        update_data['note'] = note
        agent.db.factory.save(update_data)
    #Search
    # factory_object = agent.db.factory.find().sort([("_id", 1)])

    # if factory_form.is_action(GacoiFormAction.SearchDone):
    factory_name_search = factory_form.get_field("factory_name").get_search_value()
    if factory_name_search is None:
        factory_name_search = ''
    factory_note_search = factory_form.get_field("factory_note").get_search_value()
    if factory_note_search is None:
        factory_note_search = ''

    factory_object = agent.db.factory.find({'name': {'$regex': factory_name_search}, 'note': {'$regex': factory_note_search}})\
        .sort([("_id", 1)])

    #Order
    if factory_form.order_field:
        order_field = factory_form.get_field(factory_form.order_field).get_col_database_name()
        if factory_form.order_type == 'desc':
            factory_object = factory_object.sort([(order_field, 1)])
        else:
            factory_object = factory_object.sort([(order_field, -1)])

    factory_list = list(factory_object)
    factory_form.set_form_data(factory_list)

    factory_dict = {
        "render_form_begin": factory_form.render_form_begin(),
        "render_content": factory_form.render_content(),
        "render_form_end": factory_form.render_form_end(),
        "render_scripts": factory_form.render_script(),
    }
    #endregion

    #region Location form
    #Location Form
    location_form = GacoiForm('master_location', '/master?tab_index=1&factory_id=' + str(factory_link_id)
                                                    + '&location_id=' + str(location_link_id), 'POST')
    # location_form = GacoiForm('master_location', '/master?tab_index=1', 'POST')
    location_form.set_col_database(["id, factory_id, location_name, location_note", "id, factory_id, name, note"])
    location_form.set_view("id, factory_id, location_name, location_note")
    location_form.set_key("id")

    if factory_link_id != 0:
        location_form.set_insert("location_name, location_note")
        location_form.set_default("factory_id", factory_link_id)
    else:
        location_form.set_insert("factory_id, location_name, location_note")

    location_form.set_update("factory_id, location_name, location_note")
    location_form.set_search("factory_id, location_name, location_note")
    location_form.set_order("factory_id, location_name, location_note")
    #Get dictionary for combobox
    factory_dict_info = MasterLogic.get_combobox_dict('factory')
    location_form.get_field('factory_id').set_drop_down_list_values(factory_dict_info)
    location_form.set_option_deletable(True)

    location_form.get_field('id').set_link('master?tab_index=3&location_id=[id]')
    location_form.set_required('factory_id, location_name')

    location_form.init(request)

    location_form.set_caption(["id, factory_id, location_name, location_note", "ID, Factory, Name, Note"])

    if location_form.is_action(GacoiFormAction.InsertDone):

        new_id = 1
        if agent.db.location.count() != 0:
            new_id = int(list(agent.db.location.find().sort([("_id", -1)]).limit(1))[0]['id']) + 1
        factory_id = int(location_form.get_field("factory_id").get_value())
        name = location_form.get_field("location_name").get_value()
        note = location_form.get_field("location_note").get_value()
        data_insert = {
            'id': new_id,
            'factory_id': factory_id,
            'name': name,
            'note': note
        }
        agent.db.location.insert_one(data_insert)

    if location_form.is_action(GacoiFormAction.DeleteDone):
        current_id = int(location_form.get_key_value("id"))
        data_delete = {
            'id': current_id
        }
        agent.db.location.delete_one(data_delete)
        agent.db.monitor_group_member.remove({'location_id': current_id})

        # return redirect("/role_module/0")
    if location_form.is_action(GacoiFormAction.UpdateDone):
        current_id = int(location_form.get_key_value("id"))
        factory_id = int(location_form.get_field("factory_id").get_value())
        name = location_form.get_field("location_name").get_value()
        note = location_form.get_field("location_note").get_value()

        update_data = agent.db.location.find_one({'id': current_id})
        update_data['factory_id'] = factory_id
        update_data['name'] = name
        update_data['note'] = note
        agent.db.location.save(update_data)

    # Search
    if factory_link_id != 0 and not location_form.is_action(GacoiFormAction.SearchDone):
        factory_id_search = location_form.get_field("factory_id").get_value()
    else:
        factory_id_search = location_form.get_field("factory_id").get_search_value()

    if factory_id_search is None or factory_id_search == '':
        factory_id_search = 0
        max_factory_id = MasterLogic.get_max_id('factory', 'id')
    else:
        factory_id_search = int(factory_id_search)
        max_factory_id = factory_id_search
    location_name_search = location_form.get_field("location_name").get_search_value()
    if location_name_search is None:
        location_name_search = ''
    location_note_search = location_form.get_field("location_note").get_search_value()
    if location_note_search is None:
        location_note_search = ''
    location_object = agent.db.location.find({'factory_id': {'$gte': factory_id_search, '$lte': max_factory_id},
                                              'name': {'$regex': location_name_search},
                                             'note': {'$regex': location_note_search}}).sort([("_id", 1)])

    # Order
    if location_form.order_field:
        order_field = location_form.get_field(location_form.order_field).get_col_database_name()
        if location_form.order_type == 'desc':
            location_object = location_object.sort([(order_field, 1)])
        else:
            location_object = location_object.sort([(order_field, -1)])

    location_list = agent.convert_cursor(location_object)
    # location_list = MasterLogic.get_list(location_object)
    location_form.set_form_data(location_list)

    location_dict = {
        "render_form_begin": location_form.render_form_begin(),
        "render_content": location_form.render_content(),
        "render_form_end": location_form.render_form_end(),
        "render_scripts": location_form.render_script(),
    }
    #endregion

    #region Monitor Group form

    monitor_group_form = GacoiForm('master_monitorGroup', '/master?tab_index=2&group_id=' + str(group_link_id), 'POST')
    # monitor_group_form = GacoiForm('master_monitorGroup', '/master?tab_index=2', 'POST')
    monitor_group_form.set_col_database(["id, monitor_group_name, monitor_group_note", "id, name, note"])
    monitor_group_form.set_view("id, monitor_group_name, monitor_group_note")
    monitor_group_form.set_key("id")

    monitor_group_form.set_insert("monitor_group_name, monitor_group_note")
    monitor_group_form.set_update("monitor_group_name, monitor_group_note")
    monitor_group_form.set_option_deletable(True)

    monitor_group_form.get_field('id').set_link('master?tab_index=3&group_id=[id]')
    monitor_group_form.set_required('monitor_group_name')

    monitor_group_form.set_search('monitor_group_name, monitor_group_note')
    monitor_group_form.set_order('monitor_group_name, monitor_group_note')
    monitor_group_form.init(request)

    monitor_group_form.set_caption(["id, monitor_group_name, monitor_group_note", "ID, Name, Note"])

    if monitor_group_form.is_action(GacoiFormAction.InsertDone):

        new_id = 1
        if agent.db.monitor_group.count() != 0:
            new_id = int(list(agent.db.monitor_group.find().sort([("_id", -1)]).limit(1))[0]['id']) + 1
        name = monitor_group_form.get_field("monitor_group_name").get_value()
        note = monitor_group_form.get_field("monitor_group_note").get_value()
        data_insert = {
            'id': new_id,
            'name': name,
            'note': note
        }
        agent.db.monitor_group.insert_one(data_insert)

    if monitor_group_form.is_action(GacoiFormAction.DeleteDone):
        current_id = int(monitor_group_form.get_key_value("id"))
        data_delete = {
            'id': current_id
        }
        agent.db.monitor_group.delete_one(data_delete)
        agent.db.monitor_group_member.remove({'monitor_group_id': current_id})

        # return redirect("/role_module/0")
    if monitor_group_form.is_action(GacoiFormAction.UpdateDone):
        current_id = int(monitor_group_form.get_key_value("id"))
        name = monitor_group_form.get_field("monitor_group_name").get_value()
        note = monitor_group_form.get_field("monitor_group_note").get_value()

        update_data = agent.db.monitor_group.find_one({'id': current_id})
        update_data['name'] = name
        update_data['note'] = note
        agent.db.monitor_group.save(update_data)
    # Search
    # monitor_group_object = agent.db.monitor_group.find().sort([('_id', 1)])
    # if monitor_group_form.is_action(GacoiFormAction.SearchDone):
    name_search = monitor_group_form.get_field("monitor_group_name").get_search_value()
    if name_search is None:
        name_search = ''
    note_search = monitor_group_form.get_field("monitor_group_note").get_search_value()
    if note_search is None:
        note_search = ''

    monitor_group_object = agent.db.monitor_group.find({'name': {'$regex': name_search},
                                                        'note': {'$regex': note_search}}).sort([("_id", 1)])


    # Order
    if monitor_group_form.order_field:
        order_field = monitor_group_form.get_field(monitor_group_form.order_field).get_col_database_name()
        if monitor_group_form.order_type == 'desc':
            monitor_group_object = monitor_group_object.sort([(order_field, 1)])
        else:
            monitor_group_object = monitor_group_object.sort([(order_field, -1)])

    monitor_group_list = agent.convert_cursor(monitor_group_object)
    monitor_group_form.set_form_data(monitor_group_list)

    monitor_group_dict = {
        "render_form_begin": monitor_group_form.render_form_begin(),
        "render_content": monitor_group_form.render_content(),
        "render_form_end": monitor_group_form.render_form_end(),
        "render_scripts": monitor_group_form.render_script(),
    }
    #endregion

    #region Monitor Group Member form
    #Monitor Group Member Form

    monitor_member_form = GacoiForm('master_monitorMember', '/master?tab_index=3'
                                                            '&location_id=' + str(location_link_id)
                                                            + '&group_id=' + str(group_link_id)
                                                            , 'POST')
    monitor_member_form.set_col_database(["id, monitor_group_id, location_id, sort, monitor_member_name, "
                                          "monitor_member_note",
                                          "id, monitor_group_id, location_id, sort, name, note"])
    monitor_member_form.set_view("id, monitor_group_id, location_id, sort, monitor_member_note")
    monitor_member_form.set_key("id")

    if group_link_id != 0:
        monitor_member_form.set_insert("location_id, sort, monitor_member_note")
        monitor_member_form.set_default("monitor_group_id", group_link_id)
    elif location_link_id != 0:
        monitor_member_form.set_insert("monitor_group_id, sort, monitor_member_note")
        monitor_member_form.set_default("location_id", location_link_id)
    else:
        monitor_member_form.set_insert("monitor_group_id, location_id, sort, monitor_member_note")

    monitor_member_form.set_update("monitor_group_id, location_id, sort, monitor_member_note")
    monitor_member_form.set_option_deletable(True)

    monitor_member_form.set_required('monitor_group_id, location_id, sort')

    monitor_member_form.set_search('monitor_group_id, location_id, sort, monitor_member_note')
    monitor_member_form.set_order('monitor_group_id, location_id, sort, monitor_member_note')

    monitor_member_form.set_type('sort', GacoiFormFieldType.Number)

    # Get dictionary for combobox
    group_dict_info = MasterLogic.get_combobox_dict("monitor_group")
    monitor_member_form.get_field('monitor_group_id').set_drop_down_list_values(group_dict_info)

    location_dict_info = MasterLogic.get_combobox_dict_special("location", "factory")
    monitor_member_form.get_field('location_id').set_drop_down_list_values(location_dict_info)

    monitor_member_form.init(request)

    monitor_member_form.set_caption(["id, monitor_group_id, location_id, sort, monitor_member_note",
                                    "ID, Group, Location, Sort, Note"])

    if monitor_member_form.is_action(GacoiFormAction.InsertDone):

        new_id = 1
        if agent.db.monitor_group_member.count() != 0:
            new_id = int(list(agent.db.monitor_group_member.find().sort([("_id", -1)]).limit(1))[0]['id']) + 1
        group_id = int(monitor_member_form.get_field("monitor_group_id").get_value())
        location_id = int(monitor_member_form.get_field("location_id").get_value())
        sort_value = int(monitor_member_form.get_field("sort").get_value())
        # name = monitor_member_form.get_field("monitor_member_name").get_value()
        note = monitor_member_form.get_field("monitor_member_note").get_value()
        data_insert = {
            'id': new_id,
            'monitor_group_id': group_id,
            'location_id': location_id,
            'sort': sort_value,
            # 'name': name,
            'note': note
        }
        agent.db.monitor_group_member.insert_one(data_insert)

    if monitor_member_form.is_action(GacoiFormAction.DeleteDone):
        current_id = int(monitor_member_form.get_key_value("id"))
        data_delete = {
            'id': current_id
        }
        agent.db.monitor_group_member.delete_one(data_delete)

        # return redirect("/role_module/0")
    if monitor_member_form.is_action(GacoiFormAction.UpdateDone):
        current_id = int(monitor_member_form.get_key_value("id"))
        group_id = int(monitor_member_form.get_field("monitor_group_id").get_value())
        location_id = int(monitor_member_form.get_field("location_id").get_value())
        sort_value = int(monitor_member_form.get_field("sort").get_value())
        note = monitor_member_form.get_field("monitor_member_note").get_value()

        update_data = agent.db.monitor_group_member.find_one({'id': current_id})
        update_data['monitor_group_id'] = group_id
        update_data['location_id'] = location_id
        update_data['sort'] = sort_value
        update_data['note'] = note
        agent.db.monitor_group_member.save(update_data)
    # Search

    if group_link_id != 0 and not monitor_member_form.is_action(GacoiFormAction.SearchDone):
        group_id_search = monitor_member_form.get_field("monitor_group_id").get_value()
    else:
        group_id_search = monitor_member_form.get_field("monitor_group_id").get_search_value()
    if group_id_search is None or group_id_search == '':
        group_id_search = 0
        max_group_id = MasterLogic.get_max_id('monitor_group', 'id')
    else:
        group_id_search = int(group_id_search)
        max_group_id = group_id_search

    if location_link_id != 0 and not monitor_member_form.is_action(GacoiFormAction.SearchDone):
        location_id_search = monitor_member_form.get_field("location_id").get_value()
    else:
        location_id_search = monitor_member_form.get_field("location_id").get_search_value()

    if location_id_search is None or location_id_search == '':
        location_id_search = 0
        max_location_id = MasterLogic.get_max_id('location', 'id')
    else:
        location_id_search = int(location_id_search)
        max_location_id = location_id_search

    sort_value_search = monitor_member_form.get_field("sort").get_search_value()
    if sort_value_search is None or sort_value_search == '':
        sort_value_search = 0
        max_sort_value = MasterLogic.get_max_id('monitor_group_member', 'sort')
    else:
        sort_value_search = int(sort_value_search)
        max_sort_value = sort_value_search

    note_search = monitor_member_form.get_field("monitor_member_note").get_search_value()
    if note_search is None:
        note_search = ''

    monitor_member_object = agent.db.monitor_group_member.find({
        'monitor_group_id': {'$gte': group_id_search, '$lte': max_group_id},
        'location_id': {'$gte': location_id_search, '$lte': max_location_id},
        'sort': {'$gte': sort_value_search, '$lte': max_sort_value},
        'note': {'$regex': note_search}}).sort([("_id", 1)])
    # Order
    if monitor_member_form.order_field:
        order_field = monitor_member_form.get_field(monitor_member_form.order_field).get_col_database_name()
        if monitor_member_form.order_type == 'desc':
            monitor_member_object = monitor_member_object.sort([(order_field, 1)])
        else:
            monitor_member_object = monitor_member_object.sort([(order_field, -1)])

    monitor_member_list = agent.convert_cursor(monitor_member_object)

    monitor_member_form.set_form_data(monitor_member_list)

    monitor_member_dict = {
        "render_form_begin": monitor_member_form.render_form_begin(),
        "render_content": monitor_member_form.render_content(),
        "render_form_end": monitor_member_form.render_form_end(),
        "render_scripts": monitor_member_form.render_script(),
    }

    #endregion

    #region Sensor Type form
    sensor_type_form = GacoiForm('master_sensor', '/master?tab_index=4', 'POST')
    # location_form = GacoiForm('master_location', '/master?tab_index=1', 'POST')
    sensor_type_form.set_col_database(["id, sensor_type_name, unit", "id, name, unit"])
    sensor_type_form.set_view("id, sensor_type_name, unit")
    sensor_type_form.set_key("id")

    sensor_type_form.set_insert("sensor_type_name, unit")
    sensor_type_form.set_update("sensor_type_name, unit")
    sensor_type_form.set_option_deletable(True)

    sensor_type_form.set_search('sensor_type_name, unit')
    sensor_type_form.set_order('sensor_type_name, unit')
    # factory_form.get_field('id').set_link('/master/[id]')
    sensor_type_form.set_required('sensor_type_name')

    sensor_type_form.init(request)

    sensor_type_form.set_caption(["id, sensor_type_name, unit", "ID,Name,Unit"])

    if sensor_type_form.is_action(GacoiFormAction.InsertDone):
        name = sensor_type_form.get_field("sensor_type_name").get_value()
        new_id = 1
        if agent.db.sensor_type.count() != 0:
            new_id = int(list(agent.db.sensor_type.find().sort([("_id", -1)]).limit(1))[0]['id']) + 1

        unit = sensor_type_form.get_field("unit").get_value()
        data_insert = {
            'id': new_id,
            'name': name,
            'unit': unit
        }
        agent.db.sensor_type.insert_one(data_insert)

    if sensor_type_form.is_action(GacoiFormAction.DeleteDone):
        current_id = int(sensor_type_form.get_key_value("id"))
        data_delete = {
            'id': current_id
        }
        agent.db.sensor_type.delete_one(data_delete)
        # find any location with factory_id
        delete_notify_setting_object = agent.db.notify_setting.find_one({'sensor_type_id': current_id})
        if delete_notify_setting_object is not None:
            agent.db.notify_setting.remove({'sensor_type_id': current_id})

            # TO DO delete iot collection

    if sensor_type_form.is_action(GacoiFormAction.UpdateDone):
        current_id = int(sensor_type_form.get_key_value("id"))
        name = sensor_type_form.get_field("sensor_type_name").get_value()
        unit = sensor_type_form.get_field("unit").get_value()

        update_data = agent.db.sensor_type.find_one({'id': current_id})
        update_data['name'] = name
        update_data['unit'] = unit
        agent.db.sensor_type.save(update_data)
    # Search
    # factory_object = agent.db.factory.find().sort([("_id", 1)])

    # if factory_form.is_action(GacoiFormAction.SearchDone):
    sensor_type_name_search = sensor_type_form.get_field("sensor_type_name").get_search_value()
    if sensor_type_name_search is None:
        sensor_type_name_search = ''
    sensor_unit = sensor_type_form.get_field("unit").get_search_value()
    if sensor_unit is None:
        sensor_unit = ''

    sensor_type_object = agent.db.sensor_type.find(
        {'name': {'$regex': sensor_type_name_search}, 'unit': {'$regex': sensor_unit}}) \
        .sort([("_id", 1)])

    # Order
    if sensor_type_form.order_field:
        order_field = sensor_type_form.get_field(sensor_type_form.order_field).get_col_database_name()
        if sensor_type_form.order_type == 'desc':
            sensor_type_object = factory_object.sort([(order_field, 1)])
        else:
            sensor_type_object = factory_object.sort([(order_field, -1)])

    sensor_type_list = list(sensor_type_object)
    sensor_type_form.set_form_data(sensor_type_list)

    sensor_type_dict = {
        "render_form_begin": sensor_type_form.render_form_begin(),
        "render_content": sensor_type_form.render_content(),
        "render_form_end": sensor_type_form.render_form_end(),
        "render_scripts": sensor_type_form.render_script(),
    }
    #endregion

    #region Notify form
    notify_form = GacoiForm('master_notify', '/master?tab_index=5', 'POST')
    # location_form = GacoiForm('master_location', '/master?tab_index=1', 'POST')
    notify_form.set_col_database(["id, location_id, sensor_type_id, key, min_value, max_value, remark",
                                  "id, location_id, sensor_type_id, key, minvalue, maxvalue, remark"])
    notify_form.set_view("id, location_id, sensor_type_id, key, min_value, max_value, remark")
    notify_form.set_key("id")

    notify_form.set_insert("location_id, sensor_type_id, key, min_value, max_value, remark")
    notify_form.set_update("location_id, sensor_type_id, key, min_value, max_value, remark")
    notify_form.set_search("location_id, sensor_type_id, key, min_value, max_value, remark")
    notify_form.set_order("location_id, sensor_type_id, key, min_value, max_value, remark")
    # Get dictionary for combobox
    location_dict_info = MasterLogic.get_combobox_dict_special('location', 'factory')
    notify_form.get_field('location_id').set_drop_down_list_values(location_dict_info)

    sensor_type_dict_info = MasterLogic.get_combobox_dict('sensor_type')
    notify_form.get_field('sensor_type_id').set_drop_down_list_values(sensor_type_dict_info)

    notify_form.set_option_deletable(True)

    notify_form.set_required('location_id, sensor_type_id, key, min_value, max_value')

    notify_form.init(request)

    notify_form.set_caption(["id, location_id, sensor_type_id, key, min_value, max_value, remark",
                            "ID, Location, Sensor Type, Key, Min Value, Max Value, Remark"])

    if notify_form.is_action(GacoiFormAction.InsertDone):

        new_id = 1
        if agent.db.notify_setting.count() != 0:
            new_id = int(list(agent.db.notify_setting.find().sort([("_id", -1)]).limit(1))[0]['id']) + 1
        location_id = int(notify_form.get_field("location_id").get_value())
        sensor_type_id = int(notify_form.get_field("sensor_type_id").get_value())

        key = notify_form.get_field("key").get_value()
        min_value = float(notify_form.get_field("min_value").get_value())
        max_value = float(notify_form.get_field("max_value").get_value())
        remark = notify_form.get_field("remark").get_value()
        data_insert = {
            'id': new_id,
            'location_id': location_id,
            'sensor_type_id': sensor_type_id,
            'key': key,
            'minvalue': min_value,
            'maxvalue': max_value,
            'remark': remark
        }
        agent.db.notify_setting.insert_one(data_insert)

    if notify_form.is_action(GacoiFormAction.DeleteDone):
        current_id = int(notify_form.get_key_value("id"))
        data_delete = {
            'id': current_id
        }
        agent.db.notify_setting.delete_one(data_delete)

        # return redirect("/role_module/0")
    if notify_form.is_action(GacoiFormAction.UpdateDone):
        current_id = int(notify_form.get_key_value("id"))
        location_id = int(notify_form.get_field("location_id").get_value())
        sensor_type_id = int(notify_form.get_field("sensor_type_id").get_value())
        key = notify_form.get_field("key").get_value()
        min_value = float(notify_form.get_field("min_value").get_value())
        max_value = float(notify_form.get_field("max_value").get_value())
        remark = notify_form.get_field("remark").get_value()

        update_data = agent.db.notify_setting.find_one({'id': current_id})
        update_data['location_id'] = location_id
        update_data['sensor_type_id'] = sensor_type_id
        update_data['key'] = key
        update_data['minvalue'] = min_value
        update_data['maxvalue'] = max_value
        update_data['remark'] = remark
        agent.db.notify_setting.save(update_data)

    # Search
    # location_object = agent.db.location.find().sort([("_id", 1)])
    # if location_form.is_action(GacoiFormAction.SearchDone):
    notify_location_id_search = notify_form.get_field("location_id").get_search_value()
    if notify_location_id_search is None or notify_location_id_search == '':
        notify_location_id_search = 0
        max_notify_location_id = MasterLogic.get_max_id('location', 'id')
    else:
        notify_location_id_search = int(notify_location_id_search)
        max_notify_location_id = notify_location_id_search

    sensor_type_id_search = notify_form.get_field('sensor_type_id').get_search_value()
    if sensor_type_id_search is None or sensor_type_id_search == '':
        sensor_type_id_search = 0
        max_sensor_type_id = MasterLogic.get_max_id('sensor_type', 'id')
    else:
        sensor_type_id_search = int(sensor_type_id_search)
        max_sensor_type_id = sensor_type_id_search

    notify_key_search = notify_form.get_field("key").get_search_value()
    if notify_key_search is None:
        notify_key_search = ''

    notify_min_value_search = notify_form.get_field("min_value").get_search_value()
    if notify_min_value_search is None or notify_min_value_search == '':
        notify_min_value_search = 0
        max_notify_min_value = MasterLogic.get_max_id('notify_setting', 'minvalue')
    else:
        notify_min_value_search = int(notify_min_value_search)
        max_notify_min_value = notify_min_value_search

    notify_max_value_search = notify_form.get_field("max_value").get_search_value()
    if notify_max_value_search is None or notify_max_value_search == '':
        notify_max_value_search = 0
        max_notify_max_value = MasterLogic.get_max_id('notify_setting', 'maxvalue')
    else:
        notify_max_value_search = int(notify_max_value_search)
        max_notify_max_value = notify_max_value_search

    notify_remark_search = notify_form.get_field("remark").get_search_value()
    if notify_remark_search is None:
        notify_remark_search = ''

    notify_object = agent.db.notify_setting.find({'location_id': {'$gte': notify_location_id_search,
                                                          '$lte': max_notify_location_id},
                                          'sensor_type_id': {'$gte': sensor_type_id_search,
                                                             '$lte': max_sensor_type_id},
                                          'key': {'$regex': notify_key_search},
                                          'minvalue': {'$gte': notify_min_value_search,
                                                       '$lte': max_notify_min_value},
                                          'maxvalue': {'$gte': notify_max_value_search,
                                                       '$lte': max_notify_max_value},
                                          'remark': {'$regex': notify_remark_search}}).sort([("_id", 1)])

    # Order
    if notify_form.order_field:
        order_field = notify_form.get_field(notify_form.order_field).get_col_database_name()
        if notify_form.order_type == 'desc':
            notify_object = notify_object.sort([(order_field, 1)])
        else:
            notify_object = notify_object.sort([(order_field, -1)])

    notify_list = agent.convert_cursor(notify_object)
    # location_list = MasterLogic.get_list(location_object)
    notify_form.set_form_data(notify_list)

    notify_dict = {
        "render_form_begin": notify_form.render_form_begin(),
        "render_content": notify_form.render_content(),
        "render_form_end": notify_form.render_form_end(),
        "render_scripts": notify_form.render_script(),
    }
    #endregion

    #Tab active
    tab_active_1 = ''
    tab_active_2 = ''
    tab_active_3 = ''
    tab_active_4 = ''
    tab_active_5 = ''
    tab_active_6 = ''
    # if location_link_id != 0 or group_link_id != 0:
    #     tab_active_4 = 'active'
    # else:
    if tab_index == 0:
        tab_active_1 = 'active'
    elif tab_index == 1:
        tab_active_2 = 'active'
    elif tab_index == 2:
        tab_active_3 = 'active'
    elif tab_index == 3:
        tab_active_4 = 'active'
    elif tab_index == 4:
        tab_active_5 = 'active'
    else:
        tab_active_6 = 'active'

    return render_template("master.html",
                           factory_form=factory_dict,
                           location_form=location_dict,
                           monitorGroup_form=monitor_group_dict,
                           monitorMember_form=monitor_member_dict,
                           sensor_type_form=sensor_type_dict,
                           notify_form=notify_dict,
                           tab1=tab_active_1,
                           tab2=tab_active_2,
                           tab3=tab_active_3,
                           tab4=tab_active_4,
                           tab5=tab_active_5,
                           tab6=tab_active_6,
                           location_link_id=location_link_id,
                           group_link_id=group_link_id,
                           screen_name=ScreenName.Master,
                           )


