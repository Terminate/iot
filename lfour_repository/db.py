# coding: utf-8
from sqlalchemy import BigInteger, Column, Date, DateTime, Float, ForeignKey, Integer, String, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class BillingData(Base):
    __tablename__ = 'billing_data'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    billing_date = Column(Date, nullable=False)
    billing_amount = Column(BigInteger, nullable=False)
    device_num = Column(Integer, nullable=False)
    station_num = Column(Integer, nullable=False)

    nwp = relationship('NetworkProvider')


class CommPlan(Base):
    __tablename__ = 'comm_plan'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    plan_code = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    frame_period = Column(Integer, nullable=False)
    price = Column(Integer, nullable=False)

    nwp = relationship('NetworkProvider')


class CommPlanStationGroup(Base):
    __tablename__ = 'comm_plan_station_group'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    comm_plan_id = Column(ForeignKey('comm_plan.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_group_id = Column(ForeignKey('station_group.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    comm_plan = relationship('CommPlan')
    station_group = relationship('StationGroup')


class Contract(Base):
    __tablename__ = 'contract'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    contract_code = Column(BigInteger, nullable=False)
    device_id = Column(ForeignKey('device.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    comm_plan_id = Column(ForeignKey('comm_plan.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    status = Column(Integer, nullable=False, server_default=text("'0'"))
    contract_start = Column(DateTime, nullable=False)
    contract_end = Column(DateTime, nullable=False)
    billing_start = Column(Date, nullable=False)
    billing_end = Column(Date, nullable=False)
    is_fix_station = Column(Integer, nullable=False, server_default=text("'0'"))

    comm_plan = relationship('CommPlan')
    device = relationship('Device')
    nwp = relationship('NetworkProvider')


class Device(Base):
    __tablename__ = 'device'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    frame_n = Column(Integer, nullable=False)
    slot_offset = Column(Integer, nullable=False)
    n_grid_position = Column(Integer, nullable=False)
    frame_period = Column(Integer, nullable=False, server_default=text("'36'"))
    status = Column(Integer, nullable=False)
    device_maker_id = Column(ForeignKey('device_maker.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)

    device_maker = relationship('DeviceMaker')


class DeviceMaker(Base):
    __tablename__ = 'device_maker'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    maker_id = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    address = Column(String(256), nullable=False)


class NetworkProvider(Base):
    __tablename__ = 'network_provider'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    name = Column(String(256), nullable=False)
    contact = Column(String(256), nullable=False)
    endpoint = Column(String(256), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    status = Column(Integer, nullable=False)


class RoleRight(Base):
    __tablename__ = 'role_right'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    role_id = Column(BigInteger, nullable=False)
    function_id = Column(String(50), nullable=False)
    function_name = Column(String(100), nullable=False)
    command = Column(String(10), nullable=False)


class Station(Base):
    __tablename__ = 'station'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_cluster_id = Column(Integer, nullable=False)
    station_cluster_index = Column(Integer, nullable=False)
    station_maker_id = Column(ForeignKey('station_maker.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    serial_id = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    lon = Column(Float(asdecimal=True), nullable=False)
    lat = Column(Float(asdecimal=True), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    status = Column(Integer, nullable=False)

    nwp = relationship('NetworkProvider')
    station_maker = relationship('StationMaker')


class StationGroup(Base):
    __tablename__ = 'station_group'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_group_code = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)

    nwp = relationship('NetworkProvider')


class StationGroupStation(Base):
    __tablename__ = 'station_group_station'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    station_group_id = Column(ForeignKey('station_group.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_id = Column(ForeignKey('station.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    station_group = relationship('StationGroup')
    station = relationship('Station')


class StationMaker(Base):
    __tablename__ = 'station_maker'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    maker_id = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    address = Column(String(256), nullable=False)


class User(Base):
    __tablename__ = 'user'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(BigInteger, nullable=False)
    station_group_id = Column(BigInteger, nullable=False)
    login_id = Column(String(10), nullable=False)
    password = Column(String(256), nullable=False)
