#!/usr/bin/env python3

from datetime import datetime, timezone
import paho.mqtt.client as mqtt
from core.mongoagent import agent
from config.config import MQTTBroker, PayloadMessage, DefaultValue
import time


nullCharacter = '\x00'
temperatureID = DefaultValue.Temperature.value
humidityID = DefaultValue.Humidity.value

def init():
    global temperatureID, humidityID
    temperatureObject = agent.db.sensor_type.find({"name": "Temperature"})
    if temperatureObject.count() > 0:
        temperatureID = temperatureObject.next()["id"]
    humidityObject = agent.db.sensor_type.find({"name": "Humidity"})
    if humidityObject.count() > 0:
        humidityID = humidityObject.next()["id"]


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    # client.subscribe(MQTTBroker.MQTT_TOPIC_SUBSCRIBER_2.value, MQTTBroker.MQTT_QOS_2.value)
    # client.subscribe(MQTTBroker.MQTT_TOPIC_SUBSCRIBER_3.value, MQTTBroker.MQTT_QOS_2.value)
    # client.subscribe(MQTTBroker.MQTT_TOPIC_SUBSCRIBER_4.value, MQTTBroker.MQTT_QOS_2.value)
    client.subscribe(MQTTBroker.MQTT_TOPIC_SUBSCRIBER.value, MQTTBroker.MQTT_QOS_2.value)


def on_message(client, userdata, msg):
    try:
        payload_location = msg.payload[PayloadMessage.StartLocation.value:PayloadMessage.EndLocation.value]
        payload_temperature = msg.payload[PayloadMessage.StartTemperature.value:PayloadMessage.EndTemperature.value]
        payload_humidity = msg.payload[PayloadMessage.StartHumidity.value:PayloadMessage.EndHumidity.value]
        print("temperature is " + payload_temperature.decode())
        print("humidity is " + payload_humidity.decode())
        print("location is " + str(int(payload_location.decode("utf-8").rstrip(nullCharacter).lstrip(nullCharacter))))

        location_id = int(payload_location.decode().rstrip(nullCharacter).lstrip(nullCharacter))
        temperature = float(payload_temperature.decode().rstrip(nullCharacter).lstrip(nullCharacter))
        humidity = float(payload_humidity.decode().rstrip(nullCharacter).lstrip(nullCharacter))
        # current_time = datetime.now()
        current_time = time.time()
        # sender_timestamp = float(msg.payload[15:28].decode().rstrip(nullCharacter).lstrip(nullCharacter))
        # sender_time = datetime.fromtimestamp(sender_timestamp, timezone.utc)
        temperature_data = {
            'location_id': location_id,
            'sensor_type_id': temperatureID,
            # 'sender_time': sender_time,
            'event_time': current_time,
            'value': temperature,
        }
        humidity_data = {
            'location_id': location_id,
            'sensor_type_id': humidityID,
            # 'sender_time': sender_time,
            'event_time': current_time,
            'value': humidity,
        }
        agent.db.iot.insert_one(temperature_data)
        agent.db.iot.insert_one(humidity_data)
    except:
        pass



def app_run():
    init()
    client = mqtt.Client()
    client.connect(MQTTBroker.MQTT_HOST.value, MQTTBroker.MQTT_PORT.value, MQTTBroker.MQTT_KEEPALIVE.value)
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_forever()
