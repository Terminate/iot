#!/usr/bin/env python

from lfour_repository import app

if __name__ == '__main__':
    app.secret_key = "secret_key"
    app.run(port=8080, debug=True, threaded=True)
