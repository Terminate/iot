from enum import Enum


class WebServer(Enum):
    PORT = 8080


class MongoDBServer(Enum):
    DB_HOST = '192.168.0.99'
    # DB_HOST = 'localhost'
    DB_PORT = 27017
    DB_USER = ""
    DB_PASSWORD = ""
    DB_NAME = "iot"


class MQTTBroker(Enum):
    MQTT_HOST = '192.168.0.117'
    MQTT_PORT = 8083
    MQTT_KEEPALIVE = 60
    MQTT_QOS_1 = 1
    MQTT_QOS_2 = 2
    MQTT_QOS_0 = 0
    MQTT_RETAIN = True
    MQTT_NO_RETAIN = False
    MQTT_TOPIC_SUBSCRIBER_2 = "Node2/Sensor"
    MQTT_TOPIC_SUBSCRIBER_3 = "Node3/Sensor"
    MQTT_TOPIC_SUBSCRIBER_4 = "Node4/Sensor"
    MQTT_TOPIC_SUBSCRIBER_1 = "Node1/+"
    MQTT_TOPIC_SUBSCRIBER = "Sensor/+"


class PayloadMessage(Enum):
    StartLocation = 0
    EndLocation = 4
    StartTemperature = 4
    EndTemperature = 10
    StartHumidity = 10
    EndHumidity = 15
    StartSenderTime = 15
    EndSenderTime = 28


class DefaultValue(Enum):
    Temperature = 1
    Humidity = 2
