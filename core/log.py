from decorator import decorator
from flask import request
import datetime
import re
import logging
import os
from logging.handlers import TimedRotatingFileHandler

local_ip = "127.0.0.1"
app_name = "lfour_so_api"
logger = logging.getLogger(app_name)

filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "log",
                        "{}.log".format(app_name))

handler = TimedRotatingFileHandler(filename, when="midnight", interval=1)
handler.suffix = "%Y%m%d"

# need to change the extMatch variable to match the suffix for it
handler.extMatch = re.compile(r"^\d{8}$")

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class ApiLog:
    @staticmethod
    @decorator
    def requires_log(f: callable, *args, **kwargs):
        method_name = f.__name__

        # Generate sequence ID datetime.datetime.now().timestamp()
        seq_id = "{}_{}_{}_{}".format(datetime.datetime.now().strftime("%Y%m%d.%H%M%S.%f"),
                                   request.remote_addr, local_ip, method_name)
        status = 200
        error_message = None
        trace_info = None
        ret = dict()
        ret["seq_id"] = seq_id
        try:
            res = f(*args, **kwargs)
            if isinstance(res, tuple):
                ret["data"] = res[0]
                if len(res) > 1:
                    status = res[1]
            else:
                ret["data"] = res

            if status != 200 and isinstance(ret["data"], str):
                error_message = ret["data"]

        except Exception as e:
            # Get error stack here
            import traceback
            trace_info = traceback.format_exc()
            # ret["trace_info"] = trace_info
            error_message = str(e)

            # Internal error
            status = 500

        ret["status"] = status
        ret["error_message"] = error_message

        # Print log
        logger.info(seq_id)
        logger.info(status)
        if error_message:
            logger.info(error_message)
        if trace_info:
            logger.info(trace_info)

        return ret, status