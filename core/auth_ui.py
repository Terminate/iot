from .auth import AuthBase
from core.db import User
from decorator import decorator
from flask import session
from flask import redirect
from core.db import get_db_instance, UserRole, ModuleInfo, User, RoleRight
from core.const import *

AUTH_SESSION_KEY = "_authentication"


def get_auth_instance():
    """
    :return:
    """
    from flask import g
    ret = getattr(g, '_authentication', None)
    if ret is None:
        ret = g._authentication = AuthUi()

    return ret


class AuthUi(AuthBase):
    @staticmethod
    def login(user_id, password):
        # 1) DBベースにユーザー確認
        user = AuthBase.get_user(user_id, password)
        if user is None:
            return False

        # 2) セクションに認証キー保存
        session[AUTH_SESSION_KEY] = user.id
        return True

    @staticmethod
    def logout():
        if AUTH_SESSION_KEY in session:
            session.pop(AUTH_SESSION_KEY)

    @staticmethod
    def get_user_id():
        return session[AUTH_SESSION_KEY]

    @staticmethod
    def authenticate():
        return redirect("login")

    @staticmethod
    @decorator
    def requires_auth(f: callable, *args, **kwargs):
        method_name = f.__name__

        # 認証
        # 1) セクションに認証キー確認
        if AUTH_SESSION_KEY not in session or not session[AUTH_SESSION_KEY]:
            return AuthUi.authenticate()

        user_id = session[AUTH_SESSION_KEY]

        db = get_db_instance()
        user = db.select_first(User, User.id == user_id)
        auth = get_auth_instance()

        if not user:
            return AuthUi.authenticate()

        if user.system_admin:
            # Full right
            auth.can_update = True
            auth.can_delete = True
            auth.can_insert = True
            auth.can_view = True
            return f(*args, **kwargs)

        # 権限取得／API名取得／権限確認
        module_info = db.select_first(ModuleInfo, ModuleInfo.module_function_name == method_name)
        """:type: ModuleInfo"""

        if not module_info:
            return AuthUi.authenticate()

        from sqlalchemy import join

        rr = RoleRight.table()
        ur = UserRole.table()

        roles = join(ur, rr, rr.c.role_id == ur.c.role_id).select().\
            where(rr.c.module_id == module_info.id).execute().fetchall()

        if not roles or len(roles) == 0:
            return AuthUi.authenticate()

        for role in roles:
            right = role["right"]
            if right & RoleRightType.View.value:
                auth.can_view = True
            if right & RoleRightType.Insert.value:
                auth.can_insert = True
            if right & RoleRightType.Update.value:
                auth.can_update= True
            if right & RoleRightType.Delete.value:
                auth.can_delete = True

        if not auth.can_view:
            return AuthUi.authenticate()

        return f(*args, **kwargs)

    def __init__(self):
        self.can_update = False
        self.can_delete = False
        self.can_insert = False
        self.can_view = False

    def is_update_right(self):
        return self.can_update

    def is_delete_right(self):
        return self.can_delete

    def is_insert_right(self):
        return self.can_insert


