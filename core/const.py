from enum import Enum


class UserType(Enum):
    Admin = 1
    SO = 10
    DevReg = 20


class ModuleType(Enum):
    UI = 1
    SO = 10
    DevReg = 20


class RoleRightType(Enum):
    View = 1
    Insert = 2
    Update = 4
    Delete = 8

