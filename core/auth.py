#!/usr/bin/env python3
from core.db import DbAccess, User, get_db_instance, AccessToken
import bcrypt
import uuid


class AuthBase:
    @staticmethod
    def revoke_access_token(access_token):
        """

        :param access_token:
        :return: New access token
        """
        user = AuthBase.verify_access_token(access_token)
        if user is None:
            return None
        else:
            return AuthBase.create_access_token(user.id)

    @staticmethod
    def create_access_token(user_id, expire=None):
        """
        ユーザーからアクセストークンを生成
        :param user_id:
        :type user_id: int
        :param expire:有効期限。単位は日。Noneの場合は期限がない。
        :type expire: int
        :return:
        """
        import datetime
        if expire is not None:
            expire_date = datetime.datetime.now()
            expire_date = expire_date + datetime.timedelta(days=expire)
            # TODO: 有効期限

        db = get_db_instance()
        user = db.select_first(User, User.id == user_id)

        if user is None:
            return None
        access_token = str(uuid.uuid4())

        access_token_object = db.select_first(AccessToken, AccessToken.user_id == user_id)
        # Have not yet, create
        if access_token_object is None:
            access_token_object = AccessToken()
            access_token_object.user_id = user_id
            access_token_object.access_token = access_token
            db.insert(access_token_object)
        else:
            access_token_object.access_token = access_token
            db.update()

        return access_token

    @staticmethod
    def verify_access_token(access_token):
        """

        :param access_token:
        :return:
        :rtype: User
        """
        db = get_db_instance()
        access_token_object = db.select_first(AccessToken, AccessToken.access_token == access_token)
        """:type : AccessToken"""

        if access_token_object is None:
            return None

        return access_token_object.user

    @staticmethod
    def change_password(user_id, old_password, new_password):
        db = get_db_instance()

        user = db.select_first(User, User.id==user_id)
        if not user:
            return False

        if not AuthBase.password_match(old_password, user.password):
            return False

        user.password = AuthBase.hash_password(new_password)
        db.update()

        return True

    @staticmethod
    def hash_password(password):
        """
        Encrypt password with bcrypt
        @param password:
        @return: hashed string
        """
        return bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(14))

    @staticmethod
    def password_match(password, hash_password):
        """

        :param password:
        :param hash_password:
        :return:
        """
        return bcrypt.checkpw(password.encode('utf8'), hash_password.encode('utf8'))

    @staticmethod
    def get_user(user_id, password):
        """

        :param user_id:
        :param password:
        :return:
        :rtype: User
        """
        db = get_db_instance()
        if user_id.isdigit():
            user = db.select_first(User, User.id == user_id)
        else:
            user = db.select_first(User, User.login_id == user_id)

        if user is not None:
            #if AuthBase.password_match(password, user.password):
            return user
        return None
