from pymongo import MongoClient
import re
from config.config import MongoDBServer


class MongoAgent:
    db = None
    master = None

    def __init__(self):
        client = MongoClient(MongoDBServer.DB_HOST.value, MongoDBServer.DB_PORT.value)
        self.db = client[MongoDBServer.DB_NAME.value]

        self.master = dict()

    def get_master_value(self, master_name, key):
        if master_name not in self.master:
            master = dict()
            self.master[master_name] = master
            rs = self.db[master_name].find()
            for rec in rs:
                master[rec["id"]] = rec["name"]

        master = self.master[master_name]

        if key in master:
            return master[key]
        else:
            return None

    def convert_record(self, mongo_rec):
        ret = dict()
        for key, value in mongo_rec.items():
            if key == "_id":
                continue

            match_obj = re.match(r'(.+)_id', key, re.M | re.I)

            if match_obj:
                master_name = match_obj.group(1)
                if master_name not in self.master:
                    master = dict()
                    self.master[master_name] = master
                    rs = self.db[master_name].find()
                    for rec in rs:
                        master[rec["id"]] = self.convert_record(rec)

                master = self.master[master_name]
                if value in master:
                    ret[master_name] = master[value]
                else:
                    ret[master_name] = None

            ret[key] = value
        return ret

    def convert_cursor(self, mongo_rs):
        ret = list()
        for rec in mongo_rs:
            ret.append(self.convert_record(rec))

        return ret


agent = MongoAgent()
