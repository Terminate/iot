from .auth import AuthBase
from core.db import User
from decorator import decorator
import flask
from core.db import DbAccess, get_db_instance, RoleRight, ModuleInfo, User, UserRole, RoleRight
from core.const import *


class AuthApi(AuthBase):
    @staticmethod
    def login(access_token):
        """

        :param access_token:
        :rtype: User
        """
        user = AuthApi.verify_access_token(access_token)
        if user is None:
            return False

        return user

    @staticmethod
    def authenticate():
        error_message = 'You have to login with proper credentials.'
        # Create sample data
        # 1) User
        # db = get_db_instance()
        # user = User()
        # user.name = "Dinh Hoai Vu"
        # user.user_type = UserType.SO.value
        # user.login_id = "admin"
        # user.password = AuthBase.hash_password("password")
        # user.nwp_id = 1
        # db.insert(user)
        #
        # # 2) User role
        # role = UserRole()
        # role.user_id = user.id
        # role.role_id = 1
        # db.insert(role)
        # role = UserRole()
        # role.user_id = user.id
        # role.role_id = 2
        # db.insert(role)
        #
        # # 3) Module
        # mod = ModuleInfo()
        # mod.module_function_name = "device_status_put"
        # mod.module_name = "デバイスアクティベーション"
        # mod.module_type = ModuleType.SO.value
        # db.insert(mod)
        #
        # # 4) Right
        # role_right = RoleRight()
        # role_right.role_id = 1
        # role_right.module_id = mod.id
        # role_right.right = 1
        # db.insert(role_right)
        #
        # error_message = AuthApi.create_access_token(10)

        return error_message, 401  # , {'WWW-Authenticate': 'Basic realm="Login Required"'}

    @staticmethod
    def get_access_token():
        access_token = None
        if 'Authorization' in flask.request.headers:
            access_token = flask.request.headers['Authorization']
        return access_token

    @staticmethod
    @decorator
    def requires_auth(f: callable, *args, **kwargs):
        method_name = f.__name__
        access_token = None
        if 'Authorization' in flask.request.headers:
            access_token = flask.request.headers['Authorization']
        if not access_token:
            return AuthApi.authenticate()

        user = AuthApi.login(access_token)
        if not user:
            return AuthApi.authenticate()

        # 権限取得／API名取得／権限確認
        db = get_db_instance()

        module_info = db.select_first(ModuleInfo, ModuleInfo.module_function_name == method_name)
        """:type: ModuleInfo"""

        if not module_info:
            return AuthApi.authenticate()
        from sqlalchemy import join

        rr = RoleRight.table()
        ur = UserRole.table()

        roles = join(ur, rr, rr.c.role_id == ur.c.role_id).select().\
            where(rr.c.module_id == module_info.id and rr.c.right == 1).execute().fetchall()

        if not roles or len(roles) == 0:
            return AuthApi.authenticate()

        return f(*args, **kwargs)


