
# coding: utf-8
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.engine import ResultProxy

from sqlalchemy import BigInteger, Column, Date, DateTime, Float, ForeignKey, Integer, String, Text, text, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
#
# DB_URL_DATA = "mysql+pymysql://lcp:lcp@localhost/lcpso?charset=utf8"
# DB_URL_SECRET = "mysql+pymysql://lcp:lcp@localhost/lcpso?charset=utf8"
DB_URL_DATA = "mysql+pymysql://lcpso:lcp@localhost/lcpso?charset=utf8"
DB_URL_SECRET = "mysql+pymysql://lcpso:lcp@localhost/lcpso?charset=utf8"

# http://www.rmunn.com/sqlalchemy-tutorial/tutorial.html
Base = declarative_base()
metadata = Base.metadata
engine = None


def get_db_instance(secret_database=False):
    """

    :param secret_database:
    :return:
    """
    from flask import g
    if secret_database:
        g_key = "_database"
    else:
        g_key = "_secret_database"

    ret = getattr(g, g_key, None)

    if ret is None:
        print('Create DbAccess')
        if secret_database:
            ret = g._database = DbAccess(secret_database)
        else:
            ret = g._secret_database = DbAccess(secret_database)
    else:
        print('Reuse DbAccess')

    return ret


class DbAccess:
    def __init__(self, secret_database=False):
        print("__init__")
        global engine
        global metadata

        if secret_database:
            engine = create_engine(DB_URL_SECRET, echo=True)
        else:
            engine = create_engine(DB_URL_DATA, echo=True)

        metadata.bind = engine
        self.connection = engine.connect()

        # create a configured "Session" class
        session_class = sessionmaker(bind=self.connection, autocommit=False)

        # create a Session
        self.session = session_class()

        self.trans = None
        self.metadata = metadata
        self.autocommit = True

    def __enter__(self):
        print("__enter__")

    def __exit__(self, exc_type, exc_value, traceback):
        print("__exit__")
        self.close()

    def __del__(self):
        print("__del__")

    def close(self):
        if self.session:
            self.session.close()
            self.session = None
        if self.connection:
            self.connection.close()
            self.connection = None

    def begin(self):
        if not self.trans:
            self.trans = self.connection.begin()

    def rollback(self):
        if self.trans:
            self.trans.rollback()
            self.trans = None

    def commit(self):
        if self.trans:
            self.trans.commit()
            self.trans = None

    def execute(self, query, **kwarg):
        """
        Don't execute select query here
        :param query:
        :param kwarg:
        :return:
        """
        ret = self.session.execute(query, **kwarg)

        if self.autocommit:
            self.session.commit()

        if isinstance(ret, ResultProxy):
            return ret.rowcount
        else:
            return ret

    def get_record_set(self, query, **kwarg):
        ret = self.session.execute(query, **kwarg)
        return ret.fetchall()

    def get_record(self, query, **kwarg):
        ret = self.get_record_set(query, **kwarg)
        if ret and len(ret) > 0:
            return ret[0]
        return None

    def insert(self, item):
        self.session.add(item)
        self.session.flush()
        # At this point, the object f has been pushed to the DB,
        # and has been automatically assigned a unique primary key id
        self.session.refresh(item)
        if self.autocommit:
            self.session.commit()

    def update(self):
        self.session.commit()

    def select(self, model_class, *arg):
        return self.session.query(model_class).filter(*arg)

    def delete(self, item):
        self.session.delete(item)
        self.session.commit()

    def delete_all(self, model_class, *arg):
        self.session.query(model_class).filter(*arg).delete()
        self.session.commit()

    def select_first(self, model_class, *arg):
        return self.session.query(model_class).filter(*arg).first()

    def insert_raw(self, model_class, **kwarg):
        return self.connection.execute(model_class.table().insert(), **kwarg)


class DeviceLKS(Base):
    __tablename__ = 'lks'
    id = Column(BigInteger, primary_key=True)
    device_id = Column(BigInteger)
    magic_no = Column(String(512))


class AccessToken(Base):
    __tablename__ = 'access_token'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    user_id = Column(ForeignKey('user.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    access_token = Column(String(256), nullable=False)

    user = relationship('User')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return AccessToken.__table__


class BillingData(Base):
    __tablename__ = 'billing_data'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    billing_date = Column(Date, nullable=False)
    billing_amount = Column(BigInteger, nullable=False)
    device_num = Column(Integer, nullable=False)
    station_num = Column(Integer, nullable=False)

    nwp = relationship('NetworkProvider')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return BillingData.__table__


class CommPlan(Base):
    __tablename__ = 'comm_plan'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    plan_code = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    frame_period = Column(Integer, nullable=False)
    price = Column(Integer, nullable=False)

    nwp = relationship('NetworkProvider')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return CommPlan.__table__


class CommPlanStationGroup(Base):
    __tablename__ = 'comm_plan_station_group'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    comm_plan_id = Column(ForeignKey('comm_plan.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_group_id = Column(ForeignKey('station_group.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    comm_plan = relationship('CommPlan')
    station_group = relationship('StationGroup')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return CommPlanStationGroup.__table__


class Contract(Base):
    __tablename__ = 'contract'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    contract_code = Column(BigInteger, nullable=False)
    device_id = Column(ForeignKey('device.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    comm_plan_id = Column(ForeignKey('comm_plan.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    status = Column(Integer, nullable=False, server_default=text("'0'"))
    contract_start = Column(DateTime, nullable=False)
    contract_end = Column(DateTime, nullable=False)
    billing_start = Column(Date, nullable=False)
    billing_end = Column(Date, nullable=False)
    is_fix_station = Column(Integer, nullable=False, server_default=text("'0'"))

    comm_plan = relationship('CommPlan')
    device = relationship('Device')
    nwp = relationship('NetworkProvider')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return Contract.__table__


class Device(Base):
    __tablename__ = 'device'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    frame_n = Column(Integer, nullable=False)
    slot_offset = Column(Integer, nullable=False)
    n_grid_position = Column(Integer, nullable=False)
    frame_period = Column(Integer, nullable=False, server_default=text("'36'"))
    status = Column(Integer, nullable=False)
    device_maker_id = Column(ForeignKey('device_maker.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)

    device_maker = relationship('DeviceMaker')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return Device.__table__


class DeviceMaker(Base):
    __tablename__ = 'device_maker'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    maker_id = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    address = Column(String(256), nullable=False)

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return DeviceMaker.__table__


class NetworkProvider(Base):
    __tablename__ = 'network_provider'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    name = Column(String(256), nullable=False)
    contact = Column(String(256), nullable=False)
    endpoint = Column(String(256), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    status = Column(Integer, nullable=False)

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return NetworkProvider.__table__


class ModuleInfo(Base):
    __tablename__ = 'module_info'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    module_function_name = Column(String(50), nullable=False)
    module_name = Column(String(50), nullable=False)
    module_type = Column(Integer, nullable=False)

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return ModuleInfo.__table__


class Role(Base):
    __tablename__ = 'role'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    role_name = Column(String(16), nullable=False)


class RoleRight(Base):
    __tablename__ = 'role_right'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    role_id = Column(Integer, nullable=False)
    module_id = Column(ForeignKey('module_info.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    right = Column(Integer, nullable=False)

    module = relationship('ModuleInfo')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return RoleRight.__table__


class Station(Base):
    __tablename__ = 'station'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_cluster_id = Column(Integer, nullable=False)
    station_cluster_index = Column(Integer, nullable=False)
    station_maker_id = Column(ForeignKey('station_maker.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    serial_id = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    lon = Column(Float(asdecimal=True), nullable=False)
    lat = Column(Float(asdecimal=True), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    status = Column(Integer, nullable=False)

    nwp = relationship('NetworkProvider')
    station_maker = relationship('StationMaker')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return Station.__table__


class StationGroup(Base):
    __tablename__ = 'station_group'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(ForeignKey('network_provider.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_group_code = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)

    nwp = relationship('NetworkProvider')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return StationGroup.__table__


class StationGroupStation(Base):
    __tablename__ = 'station_group_station'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    station_group_id = Column(ForeignKey('station_group.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    station_id = Column(ForeignKey('station.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    station_group = relationship('StationGroup')
    station = relationship('Station')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return StationGroupStation.__table__


class StationMaker(Base):
    __tablename__ = 'station_maker'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    maker_id = Column(Integer, nullable=False)
    name = Column(String(256), nullable=False)
    address = Column(String(256), nullable=False)

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return StationMaker.__table__


class User(Base):
    __tablename__ = 'user'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    nwp_id = Column(BigInteger, nullable=False)
    user_type = Column(Integer, nullable=False)
    name = Column(String(16), nullable=False)
    login_id = Column(String(20), nullable=False)
    password = Column(String(256), nullable=False)
    system_admin = Column(Boolean, nullable=True)

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return User.__table__

    def __repr__(self):
        return '<User %r>' % self.id


class UserRole(Base):
    __tablename__ = 'user_role'

    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
    modified = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    upd_count = Column(Integer, nullable=False, server_default=text("'1'"))
    note = Column(Text)
    user_id = Column(ForeignKey('user.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    role_id = Column(ForeignKey('role.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    role = relationship('Role')
    user = relationship('User')

    @staticmethod
    def table():
        """
        :rtype: Table
        """
        return UserRole.__table__
