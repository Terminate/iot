#!/usr/bin/env python

from lfour_repository import app
from config.config import WebServer

if __name__ == '__main__':
    app.secret_key = "secret_key"
    app.run(host="0.0.0.0",port=WebServer.PORT.value, debug=True, threaded=True)

