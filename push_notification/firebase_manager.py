import logging
import threading

import firebase_admin as fb

from datetime import datetime, timedelta

from firebase_admin import credentials
from firebase_admin import messaging
from firebase_admin.messaging import ApiCallError


from core.mongoagent import agent

TAG = "FireBaseManager"

ACCOUNT_KEY_PATH = 'push_notification/account_key/serviceAccountKey.json'
IOT_CLOUD_PROJECT = 'iotnidec-1f989'

INTERVAL = 5  # 05s

PAST_INTERVAL = 10 #
"""
FCM manager:
    1. Push message: list of keys and information of temperature and humidity sensor
    2. Push timer: start and cancel timer
"""


def register_firebase():
    cred = credentials.Certificate(ACCOUNT_KEY_PATH)
    iot_app = fb.initialize_app(cred)
    logging.info('{0} {1}'.format(TAG, iot_app.name))


def start_timer(interval, target):
    timer = threading.Timer(INTERVAL, timer_func, [interval, target])
    timer.start()


def timer_func(interval, target):
    try:
        target()
    except Exception as e:
        logging.error('{}'.format(e))
    finally:
        recur_timer = threading.Timer(interval, timer_func, [interval, target])
        recur_timer.start()


# send message via FCM
def push_message(topic, data, title, body):
    try:
        message = messaging.Message(
            data = data,
            notification=messaging.Notification(
                title=title,
                body=body,
            ),
            topic=topic
        )
        ret = messaging.send(message)
    except (ApiCallError, ValueError) as e:
        logging.error('{0} {1}'.format(TAG, e))
    else:
        logging.info('{0} {1}'.format(TAG, ret))


def send_sensor_data():
    topics = agent.db.notify_setting.find()
    data = dict()
    for topic in topics:
        try:
            location = agent.db.location.find({"id": topic["location_id"]}).next()
            factory = agent.db.factory.find({"id": location["factory_id"]}).next()
            sensors = agent.db.iot.find({"location_id": topic["location_id"],
                                         "sensor_type_id": topic["sensor_type_id"]}).sort([("event_time", -1)]).next()
            sensor_value = sensors["value"]

            current_time = datetime.now() - timedelta(seconds=PAST_INTERVAL)
            event_time = sensors["event_time"]

            if (sensor_value < topic["minvalue"] or sensor_value > topic["maxvalue"]) \
                    and current_time <= event_time:
                data["key"] = topic["key"]
                data["location"] = location["name"]
                data["factory"] = factory["name"]
                data["sensor_type"] = str(topic["sensor_type_id"])
                data["value"] = str(sensor_value)

                title = data["factory"] + "/" + data["location"]
                body = ""
                if topic["sensor_type_id"] == 1:
                    body = "Temperature: " + data["value"] + " " + "°C"
                elif topic["sensor_type_id"] == 2:
                    body = "Humidity: " + data["value"] + " " + "%"

                push_message(topic["key"], data, title, body)

        except Exception as e:
            pass


def run():
    register_firebase()
    start_timer(INTERVAL, send_sensor_data)